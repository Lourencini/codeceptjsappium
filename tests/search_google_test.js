Feature('Search Google')

Scenario('Search QAzando', ({I}) => {
  I.amOnPage('https://www.google.com.br')
  I.fillField('input[class="gLFyf"]', 'QAzando')
  I.waitForElement('li[class="sbct"]', 5)
  I.click('li[class="sbct"]')
  I.see('qazando.com.br')
})