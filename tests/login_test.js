Feature('login')

const { I, login_page, home_page } = inject();

BeforeSuite(() => {
  console.log('BeforeSuit')
})

Before(() => {
  console.log('Before')
})

Scenario('Login with success', () => {

  I.runOnIOS(() => {
    console.log('Estou no IOS')
  })

  I.runOnAndroid(() => {
    login_page.doLogin('teste@teste.com', '123456')
    //pause() // Debug interativo, tudo executado aqui, vai para um arquivo, cli-history em output
    //home_page.retry(2).checkLoginSuccess() // utilizando retry 2x
    home_page.checkLoginSuccess()
  })
});

Scenario('Login with error', () => {
  login_page.doLogin('teste@testes.com', '123456')
  login_page.checkLoginError()
});

AfterSuite(() => {
  console.log('AfterSuit')
})

After(() => {
  console.log('After')
})