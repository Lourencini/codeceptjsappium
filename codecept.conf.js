const server = require('./server/server.js')

const {
  setHeadlessWhen,
  setCommonPlugins
} = require('@codeceptjs/configure');
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

/** @type {CodeceptJS.MainConfig} */
exports.config = {
  name: 'qazando-automation',
  tests: './tests/*_test.js',
  output: './output',
  helpers: {
    Appium: {
      platform: 'Android',
      // app: './apk/app-release.apk',
      desiredCapabilities: {
        device: 'pixel3', //nome do emulador
        platformVersion: '10', //versão do android
        // appPackage: 'com.qazandoapp', // packageActivity
        // appActivity: 'MainActivity', // tela inicial do projeto
        automationName: 'UiAutomator2',
        chromedriverExecutable: './utils/chromedriver/chromedriver',
        browserName: 'Chrome'
      }
    }
  },
  include: {
    I: './steps_file.js',
    login_page: './pages/login_page.js',
    home_page: './pages/home_page.js',
  },
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  bootstrap: async () => {
    //hook que executa antes de tudo
    await server.start()
  },
  teardown: async () => {
     //hook que executa após tudo
    await server.stop()
  },
  mocha: {},
  plugins: {
    retryFailedStep: {
      enable: true
    },
    screenshotOnFail: {
      enable: true
    }, 
    allure: {
      enabled: true,
      require: '@codeceptjs/allure-legacy',
    }
  }
}