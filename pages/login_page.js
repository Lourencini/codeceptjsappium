const { I } = inject();

module.exports = {

  fields: {
    email: '~email',
    password: '~senha'
  },

  buttons: {
    enter: '~entrar'
  },

  menssages: {
    login_error: '~lognFail'
  },

  doLogin(email, password) {
    I.wait(3)
    I.fillField(this.fields.email, email)
    I.fillField(this.fields.password, password)
    I.tap(this.buttons.enter)
  },

  checkLoginError() {
    I.waitForElement(this.menssages.login_error, 5)
    I.seeElement(this.menssages.login_error)
  }
}
