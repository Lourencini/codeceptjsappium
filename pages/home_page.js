const { I } = inject();

module.exports = {
  button: {
    save: '~salvar',
  },

  fields: {
    code: { 
      android: '~codigo',
      ios: '' },
    name: { 
      android: '~aluno',
      ios: '' },
    search: { 
      android: '~search',
      ios: '' },
  },

  registerStudent(code, name) {
    I.waitForElement(this.fields.code, 5)
    I.fillField(this.fields.code, code)
    I.fillField(this.fields.name, name)
    I.tap(this.button.save)
  },

  searchStudent(search, check) {
    I.fillField(this.fields.search, search)

    I.runOnAndroid( () => {
      I.seeElement(`//android.view.ViewGroup[@content-desc="${check}"]/android.widget.TextView`)
    })
    
    I.runOnIOS(() => {
      I.seeElement(``)
    })
  },

  fillSearch(search) {
    I.fillField(this.fields.search, search)
  },

  checkLoginSuccess() {
    I.waitForElement(this.button.save, 5)
    I.seeElement(this.button.save)
  },
}