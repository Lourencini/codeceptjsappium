***Anotações Gerais***

Appium-doctor

Ele verifica se tudo que precisamos para executar o appium está instalado no pc.
Execute o comando para instalação:

npm i -g appium-doctor

Agora que temos o appium-doctor instalado, execute o comando para verificação:

appium-doctor

Agora vamos instalar o appium, então execute o comando:

npm i -g appium

instalar o appium desktop, não instalei no linux ainda

Agora vamos instalar o android studio

https://developer.android.com/studio?gclid=CjwKCAjw0N6hBhAUEiwAXab-TSkPWvGiiNPcy_WfvunPyl2aV6-eTUBKz8P_r2n1wJNz3-amlss9YhoC0UAQAvD_BwE&gclsrc=aw.ds

Para instalar o andoid studio no ubuntu, descompacte o arquivo baixado, abra o terminal em /bin e execute o ./studio.sh

Agora com tudo instalado vamos configurar as variáveis de ambiente dentro do nosso bashrc

vim ~/.bash

Adicione as linhas:

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/tools
export JAVA_HOME="/usr/lib/jvm/default-java"

Feche o terminal e abra novamente, execute o comando printenv para verificar se a variável de ambiente ANDROID_HOME está aparecendo.
=================================================================================

***Appium Inspector***

Baixe a versão desejada no link

https://github.com/appium/appium-inspector

No linux baixei ``Appium-Inspector-linux-2023.2.1.AppImage``, execute o comando:

chmod a+x Appium-Inspector-linux-2023.2.1.AppImage

Agora para executar:

./Appium-Inspector-linux-2023.2.1.AppImage

Vídeos para auxilio:
https://www.youtube.com/watch?v=7Qf1IPONkyw

=================================================================================

***Mac***

Vamos realizar o mesmo que no ubuntu, porém vamos usar o comando:

nano .bash_profile o arquivo será criado caso não exista.

Adicione as linhas:

export ANDROID_HOME=/Users/$USER/Library/Android/sdk
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/tools

export JAVA_HOME=$(/usr/libexec/java_home)
export PATH=${JAVA_HOME}/bin:$PATH

save o arquivo e digite o comando source .bash_profile

=================================================================================

Caso falte o arquivo ANDROID no SDK, veja os vídeos

https://www.youtube.com/watch?v=yev_4WmnRhI
https://www.youtube.com/watch?v=glPjT27xWpA&t=70

Abre o Android Studio >> More Actions >> SDK Manager >> SDK Tools >> Desmaque a opção Hide Obsolete Packages e procure na lista "Android SDK Tools(Obsolete) e instale.

=================================================================================

***Emuladores***

Após criar o emulador no Androit Stuio e iniciar, execute o comando:

adb devices

Você deve ter o retorno:

List of devices attached
emulator-5554	device

``Caso não apareça nada, verifique se a opção de desenvolvedor está ativa e a opção USB debugging está habilitada.``

=================================================================================

***Agora vamos instalar o Codceptjs***

npm install codeceptjs webdriverio --save

Após a instalação, vamos iniciar as configuração do CodeceptJS com o comando:

npx codeceptjs init

<img src="readme_images/configCodeceptjs.png">

***Para executar nosso projeto para Android***

PLATFORM=Android APP='./apk/app-release.apk' DEVICE=pixel VERSION=9 PACKAGE=com.qazandoapp ACTIVITY=MainActivity npx codeceptjs run --steps

***Para executar para o iOS***

PLATFORM=iOS APP='./apk/qazando.app' DEVICE='iPhone Pro Max' VERSION=14.4 npx codeceptjs run --steps

=================================================================================

***Seletores***

ID = #id
accessibilityId = ~email



=================================================================================
***Bootstrap & teardown***

Essas opções estão no codecept.conf.js

  bootstrap: async () => {
    //hook que executa antes de tudo
    await server.start()
  },
  teardown: async () => {
     //hook que executa após tudo
    await server.stop()
  }

=================================================================================
***Gherkin***

Execute o comando:

npx codeceptjs gherkin:init

Verifique as alterações que aconteceram no condecept.conf.js

Adicione no condecept.conf em include:

 gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
 }

 Para gerar os steps do gherkin, execute o comando:
 
 npx codeceptjs gherkin:snippets

=================================================================================

  Appium: {
      platform: process.env.PLATFORM,
      app: process.env.APP,
      desiredCapabilities: {
        device: process.env.DEVICE, //nome do emulador
        platformVersion: process.env.VERSION, //versão do android
        appPackage: process.env.PLATFORM == 'Android' ? process.env.PACKAGE : "", // packageActivity
        appActivity: process.env.PLATFORM == 'Android' ? process.env.ACTIVITY : "" // tela inicial do projeto
      }
    }

=================================================================================

Execução do projeto

Como configuramos dois scripts para execução dos testes android e iOS, para executar o projeto execute os comandos:

npm run android
npm run ios