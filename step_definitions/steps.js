const { I } = inject();

Given('I fill email', () => {
  I.wait(3)
  I.fillField('~email', 'teste@teste.com')
})

Given('I fill password', () => {
  I.fillField('~senha', '123456')
})

When('I tap on Entrar', () => {
  I.tap('~entrar')
})

Then('I see the button Salvar', () => {
  I.waitForElement('~salvar', 3)
  I.seeElement('~salvar')
})